(function () {
    document.addEventListener('DOMContentLoaded', event => {
        fetchContacts();
    });

    function fetchContacts() {
        // fetch all the database entries from contact/all
        fetch('./contacts/all')
            .then(response => response.json())
            .then(populateContactTable);
    }

    function populateContactTable(items) {
        // looping all the database items
        for (let item of items) {
            appendContactTable(item);
        }

    }

    function appendContactTable(item) {
        // making a row with 3 cells for every entry
        let table = document.getElementById('contacts_table').getElementsByTagName('tbody')[0];
        let row = table.insertRow();
        addTextCell(row, item.name);
        addTextCell(row, item.codeName);
        addTextCell(row, item.phone);
    }

    function addTextCell(row, value) {
        // inserts a cell to a row
        let cell = row.insertCell();
        cell.appendChild(document.createTextNode(value));
    }

})();