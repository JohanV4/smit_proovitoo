import sqlite3
import json
import datetime
import collections
import os
import jwt
from functools import wraps

from flask import Flask, request, jsonify, render_template, redirect

app = Flask(__name__)
app.config['SECRET_KEY'] = 'salakoodjwtjaoks'
app.config['JWT'] = ""


def token_required(functs):
    @wraps(functs)
    def decorated(*args, **kwargs):
        """
        Check if token exists and is correct.
        """
        token = request.args.get('token')

        if not token:
            return jsonify({'message': 'Token on puudu!'}), 403
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return jsonify({'message': 'Token on vale!'}), 403

        return functs(*args, **kwargs)

    return decorated


def connection(database_file):
    """ create a database connection to the SQLite database specified by the database_file.
    :param database_file: database file location
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(database_file, check_same_thread=False)

        return conn

    except sqlite3.Error as e:
        print(e)

    return None


database = os.path.join(os.path.dirname(__file__), 'data')
connect = connection(database)
cursor = connect.cursor()


def dict_factory(cursor, row):
    """
    Helps to create a dictionary
    :param cursor:
    :param row:
    :return:
    """
    d = {}

    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]

    return d


def add_contact_to_db(name, code_name, number):
    """
    Add contact to the database.
    :param name: contact's name
    :param code_name: contact's code-name
    :param number: contact's number
    :return: none
    """
    cursor.execute('''INSERT INTO contacts(name, codeName, number) VALUES(?,?,?)''', (name, code_name, number))
    connect.commit()


@app.route("/", methods=['GET'])
def login():
    """
    Login page view.
    :return: renders login page
    """
    return render_template("login.html")


@app.route("/main", methods=['GET'])
@token_required
def main():
    """
    Main page view.
    :return: renders main page
    """
    return render_template("index.html")


@app.route("/contacts/all", methods=['GET'])
def all_users_api():
    """
    All contacts in database.
    :return: json dict of all contacts
    """
    connect.row_factory = dict_factory
    all_contacts = cursor.execute('SELECT * FROM contacts;').fetchall()
    objects_list = []

    for row in all_contacts:
        d = collections.OrderedDict()
        d['id'] = row[0]
        d['name'] = row[1]
        d['codeName'] = row[2]
        d['phone'] = row[3]
        objects_list.append(d)

    j = json.dumps(objects_list)

    return j


@app.route('/contacts', methods=['GET'])
def api_filter():
    """
    Filtered contacts based on the query parameters.
    :return: json dict of filtered contacts
    """
    query_parameters = request.args

    id = query_parameters.get('id')
    name = query_parameters.get('name')
    codeName = query_parameters.get('codename')
    number = query_parameters.get('phone')

    query = "SELECT * FROM contacts WHERE"
    to_filter = []

    if id:
        query += ' id=? AND'
        to_filter.append(id)
    if name:
        query += ' name=? AND'
        to_filter.append(name)
    if codeName:
        query += ' codename=? AND'
        to_filter.append(codeName)
    if number:
        query += ' number=? AND'
        to_filter.append(number)

    query = query[:-4] + ';'
    connect.row_factory = dict_factory
    results = cursor.execute(query, to_filter).fetchall()
    objects_list = []

    for row in results:
        d = collections.OrderedDict()
        d['id'] = row[0]
        d['name'] = row[1]
        d['codeName'] = row[2]
        d['phone'] = row[3]
        objects_list.append(d)
    j = json.dumps(objects_list)

    return j


@app.route("/add_contact", methods=['POST'])
def add_contact():
    """
    Add contact to the database.
    :return: redirects to the main page
    """
    if request.form:
        add_contact_to_db(request.form['name'], request.form['codeName'], request.form['phone'])
    elif request.get_json():
        json_contact = request.get_json()
        values_list = list(json_contact.values())
        add_contact_to_db(values_list[0], values_list[1], values_list[2])
    return redirect("main?token=" + app.config['JWT'], code=303)


@app.route('/login_validation', methods=['POST'])
def login_validation():
    """
    Check if password and username are correct and create the JSON Web Token
    :return: if everything is correct redirects to the main page with token else redirects back to the login page
    """
    auth = request.form

    if auth and request.form['pswrd'] == 'SeeOnSalajane!' and request.form['username'] == 'smit':
        token = jwt.encode(
            {'user': request.form['username'], 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=20)},
            app.config['SECRET_KEY'])
        app.config['JWT'] = token.decode('UTF-8')

        return redirect("main?token=" + token.decode('UTF-8'), code=303)

    return redirect(request.referrer, 403)


if __name__ == '__main__':
    app.run()  #
